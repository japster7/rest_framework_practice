from django.urls import path

from chat import views as cv

app_name = 'chat'
urlpatterns = [
    path('', cv.index, name='index'),
    path('<slug:room_name>/', cv.room, name='room'),
]