from django.urls import path

from chat import consumers as cc


websocket_urlpatterns = [
    path('ws/chat/<slug:room_name>/', cc.ChatConsumer),
]
