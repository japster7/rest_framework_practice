from django.http import Http404
from django.shortcuts import render

from rest_framework import views as rv, response as rr, status as rs

from snippets import models as sm, serializers as ss


class SnippetList(rv.APIView):
    def get(self, request, format=None):
        snippets = sm.Snippet.objects.all()
        serializer = ss.SnippetSerializer(snippets, many=True)
        return rr.Response(serializer.data)

    def post(self, request, format=None):
        serializer = ss.SnippetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return rr.Response(serializer.data, status=rs.HTTP_201_CREATED)
        return rr.Response(serializer.errors, status=rs.HTTP_400_BAD_REQUEST)

class SnippetDetail(rv.APIView):
    def get_object(self, pk):
        try:
            return ss.Snippet.objects.get(pk=pk)
        except Snippet.DoesNotExist:
            raise Http404()

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ss.SnippetSerializer(snippet)
        return rr.Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ss.SnippetSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return rr.Response(serializer.data)
        return rr.Response(serializer.errors, status=rs.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return rr.Response(status=rs.HTTP_204_NO_CONTENT)
