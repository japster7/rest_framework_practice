from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from snippets import views as sv


app_name = 'snippets'
urlpatterns = [
    path('', sv.SnippetList.as_view(), name='list'),
    path('<int:pk>/', sv.SnippetDetail.as_view(), name='detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
