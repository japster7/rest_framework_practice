from rest_framework import serializers as rs

from snippets import models as sm


class SnippetSerializer(rs.ModelSerializer):
    class Meta:
        model = sm.Snippet
        fields = '__all__'